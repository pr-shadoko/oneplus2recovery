package com.raaabbit;

import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OnePlus2Recovery {
    static final Pattern LOCAL_SIZE_EXTRACT_PATTERN = Pattern.compile("^(?:[^ ]* *){4}([0-9]*) *.*$");
    static final Pattern REMOTE_SIZE_EXTRACT_PATTERN = Pattern.compile("^(?:[^ ]* *){3}([0-9]*) *.*$");
    static final Pattern REMOTE_SPLIT_CHUNK_PATTERN = Pattern.compile("^(/?(?:[^/]*/)*)([^/]*)$");

    static final String CHUNK_PREFIX = "op2rec_";
    static final String DEFAULT_REMOTE_PATH = "/storage/emulated/0/";
    static final String DEFAULT_LOCAL_DIR_PATH = "./op2rec_backup/";
    static final int DEFAULT_TIMEOUT = 120;
    static final int DEFAULT_CHUNK_SIZE = 15 * 1048576;//15MB

    String remotePath;
    String localDirPath;
    int timeout;
    int chunkSize;

    public static void main(String[] args) {
//            System.out.println("Usage: java -jar op2rec.jar REMOTE_PATH [LOCAL_DIR_PATH [TIMEOUT [CHUNK_SIZE]]]");
        String remotePath = DEFAULT_REMOTE_PATH;
        if (args.length < 1) {
            System.out.println("No REMOTE_PATH given. Assuming '" + DEFAULT_REMOTE_PATH + "'");
        } else {
            remotePath = args[0];
        }

        String localDirPath = DEFAULT_LOCAL_DIR_PATH;
        if (args.length < 2) {
            System.out.println("No LOCAL_DIR_PATH given. Assuming '" + DEFAULT_LOCAL_DIR_PATH + "'");
        } else {
            localDirPath = args[1];
        }

        int timeout = DEFAULT_TIMEOUT;
        if (args.length >= 3) {
            try {
                timeout = Integer.valueOf(args[2]);
            } catch (NumberFormatException e) {
                System.err.println("Invalid timeout value. Using default: " + DEFAULT_CHUNK_SIZE + "s");
            }
        }
        int chunkSize = DEFAULT_CHUNK_SIZE;
        if (args.length >= 4) {
            try {
                chunkSize = Integer.valueOf(args[3]);
            } catch (NumberFormatException e) {
                System.err.println("Invalid chunck size value. Using default: " + DEFAULT_CHUNK_SIZE + "B");
            }
        }

        OnePlus2Recovery op2rec = new OnePlus2Recovery(remotePath, localDirPath, timeout, chunkSize);
        op2rec.recover();
    }

    OnePlus2Recovery(String remotePath, String localDirPath, int timeout, int chunkSize) {
        this.remotePath = remotePath;
        this.localDirPath = localDirPath;
        if (!this.localDirPath.endsWith("/")) {
            this.localDirPath += "/";
        }
        this.timeout = timeout;
        this.chunkSize = chunkSize;
    }

    void recover() {
        try {
            Deque<String> smallFiles = retrieveSmallFilesList(remotePath, chunkSize);
            getFiles(smallFiles);
            Deque<String> largeFiles = retrieveLargeFilesList(remotePath, chunkSize);
            getLargeFiles(largeFiles);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    Deque<String> retrieveSmallFilesList(String dir, int size) throws IOException {
        final String[] CMD_LIST_FILES = {"adb", "shell", String.format("find '%s' -type f -size -%dc", dir, size)};
        System.out.println("Recovering small files list:");
        return retrieveFilesList(CMD_LIST_FILES);
    }

    Deque<String> retrieveLargeFilesList(String dir, int size) throws IOException {
        final String[] CMD_LIST_FILES = {"adb", "shell", String.format("find '%s' -type f -size +%dc", dir, size)};
        System.out.println("Recovering big files list:");
        return retrieveFilesList(CMD_LIST_FILES);
    }

    Deque<String> retrieveFilesList(String[] cmd) throws IOException {
        Deque<String> bigFiles = new ArrayDeque<>();
        Process processList = new ProcessBuilder(cmd).start();
        BufferedReader reader = new BufferedReader(new InputStreamReader(processList.getInputStream()));
        String remoteFilePath;
        while ((remoteFilePath = reader.readLine()) != null) {
            System.out.println("READ (" + bigFiles.size() + "): " + remoteFilePath);
            String localFilePath = localDirPath + remoteFilePath;
            File parentDirectory = new File(localFilePath).getParentFile();
            if (!parentDirectory.exists()) {
                parentDirectory.mkdirs();
            }
            bigFiles.add(remoteFilePath);
        }
        reader.close();
        return bigFiles;
    }


    void getLargeFiles(Deque<String> largeFiles) {
        int largeFilesCount = largeFiles.size(), i = 1, localSize, remoteSize;
        boolean download;
        while (!largeFiles.isEmpty()) {
            System.out.println(String.format("GET LARGE: [%d / %d]", i, largeFilesCount));
            String largeRemoteFilePath = largeFiles.remove();
            String largeLocalFilePath = localDirPath + largeRemoteFilePath;
            File file = new File(largeLocalFilePath);
            if (file.exists()) {// Check if it has to be downloaded
                try {
                    localSize = getLocalFileSize(largeLocalFilePath);
                    remoteSize = getRemoteFileSize(largeRemoteFilePath);
                } catch (IOException | InterruptedException | ProcessFailureException e) {// Cannot retrieve file size: retry later
                    e.printStackTrace();
                    largeFiles.add(largeRemoteFilePath);
                    continue;
                }

                if (localSize == remoteSize) {// File must have already been downloaded correctly
                    i++;
                    download = false;
                } else {
                    download = true;
                }
            } else {
                download = true;
            }

            if(download) {
                System.out.println("SPLIT");
                Deque<String> chunksDownload = splitLargeFile(largeRemoteFilePath);
                if (chunksDownload == null) {
                    System.out.println("LARGE FAILED");
                    largeFiles.add(largeRemoteFilePath);
                    continue;
                }
                System.out.println("DOWNLOAD");
                List<String> chunksMerge = new ArrayList<>(chunksDownload);
                getFiles(chunksDownload);
                Collections.sort(chunksMerge);
                if (!mergeChunks(chunksMerge, largeRemoteFilePath)) {
                    System.out.println("Merge failed: " + largeRemoteFilePath);
                }
            } else {
                System.out.println("LARGE SKIPPED");
            }
        }
    }

    void getFiles(Deque<String> files) {
        int filesCount = files.size(), i = 1, localSize, remoteSize;
        while (!files.isEmpty()) {
            String remoteFilePath = files.remove();
            String localFilePath = localDirPath + remoteFilePath;
            System.out.println("GET [" + i + "/" + filesCount + "]: " + localFilePath + " ");
            File file = new File(localFilePath);
            if (file.exists()) {// Check if it has to be downloaded
                try {
                    localSize = getLocalFileSize(localFilePath);
                    remoteSize = getRemoteFileSize(remoteFilePath);
                } catch (IOException | InterruptedException | ProcessFailureException e) {// Cannot retrieve file size: retry later
                    e.printStackTrace();
                    files.add(remoteFilePath);
                    continue;
                }

                if (localSize == remoteSize) {// File must have already been downloaded correctly
                    i++;
                    System.out.println("SKIPPED");
                } else {// Download the file
                    if (getFile(remoteFilePath)) {
                        System.out.println("OK");
                        i++;
                    } else {
                        System.out.println("FAILED");
                        files.add(remoteFilePath);
                    }
                }
            } else {// Download the file
                if (getFile(remoteFilePath)) {
                    System.out.println("OK");
                    i++;
                } else {
                    System.out.println("FAILED");
                    files.add(remoteFilePath);
                }
            }
        }
    }

    boolean getFile(String remoteFilePath) {
        String localFilePath = localDirPath + remoteFilePath;

        final String[] CMD_GET_FILE = {"adb", "pull", remoteFilePath, localFilePath};
        try {
            Process processGet = new ProcessBuilder(CMD_GET_FILE).start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(processGet.getErrorStream()));
            String processResult;
            while ((processResult = reader.readLine()) != null) {
                System.out.println(processResult);
            }
            if (processGet.waitFor(timeout, TimeUnit.SECONDS)) {
                System.out.println("command completed");
                reader.close();
                return processGet.exitValue() == 0;
            } else {
                System.out.println("command timed out");
                return false;
            }
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    int getLocalFileSize(String localFilePath) throws IOException, InterruptedException, ProcessFailureException {
        final String[] CMD_FILE_SIZE_LOCAL = {"ls", "-l", localFilePath};
        return getFileSize(CMD_FILE_SIZE_LOCAL, LOCAL_SIZE_EXTRACT_PATTERN);
    }

    int getRemoteFileSize(String remoteFilePath) throws IOException, InterruptedException, ProcessFailureException {
        final String[] CMD_FILE_SIZE_REMOTE = {"adb", "shell", String.format("ls -l '%s'", remoteFilePath)};
        return getFileSize(CMD_FILE_SIZE_REMOTE, REMOTE_SIZE_EXTRACT_PATTERN);
    }

    int getFileSize(String[] command, Pattern extractPattern) throws IOException, InterruptedException, ProcessFailureException {
        Process processSize = new ProcessBuilder(command).start();
        BufferedReader reader = new BufferedReader(new InputStreamReader(processSize.getInputStream()));
        String processResult = reader.readLine();
        if (processSize.waitFor() == 0) {
            Matcher matcher = extractPattern.matcher(processResult);
            int size = -1;
            if (matcher.matches()) {
                try {
                    size = Integer.valueOf(matcher.group(1));
                } catch (NumberFormatException e) {
                    throw new ProcessFailureException("Process failed: [NumberFormatException] " + processResult);
                }
            }
            reader.close();
            return size;
        } else {
            reader.close();
            throw new ProcessFailureException("Process failed: [exit=" + processSize.exitValue() + "]" + processResult);
        }
    }

    Deque<String> splitLargeFile(String remoteFilePath) {
        Deque<String> chunks = new ArrayDeque<>();
        try {
            int remoteSize = getRemoteFileSize(remoteFilePath);
            int partsCount = (int) Math.ceil((double) remoteSize / (double) chunkSize);
            Matcher matcher = REMOTE_SPLIT_CHUNK_PATTERN.matcher(remoteFilePath);
            String name;
            if (matcher.matches()) {
                name = matcher.group(1) + CHUNK_PREFIX + matcher.group(2);
                final String[] CMD_SPLIT_FILE = {"adb", "shell", "split", "-b", String.valueOf(chunkSize), remoteFilePath, name};
                Process processSplit = new ProcessBuilder(CMD_SPLIT_FILE).start();
                if (processSplit.waitFor() == 0) {
                    for (int i = 0; i < partsCount; i++) {
                        chunks.add(name + litteralSuffix(i));
                    }
                } else {// Split error: retry later
                    return null;
                }
            }
        } catch (IOException | InterruptedException | ProcessFailureException e) {// Process error: retry later
            return null;
        }
        return chunks;
    }

    static String litteralSuffix(int i) {// build the litteral suffix 'aa', 'ab', etc.
        char a = 'a';
        return String.valueOf((char) (a + i / 26)) + String.valueOf((char) (a + i % 26));
    }

    boolean mergeChunks(List<String> chunks, String remotePath) {
        System.out.println("MERGE: " + remotePath);
        byte[] buffer = new byte[4096];
        int readBytesCount;
        try {
            File out = new File(localDirPath + remotePath);
            if(!out.exists()) {
                out.createNewFile();
            }
            System.out.println("Opening output file: " + out.getAbsolutePath());
            FileOutputStream fos = new FileOutputStream(out);
            try {
                for (String c : chunks) {
                    System.out.println("Opening input file: " + c);
                    File in = new File(localDirPath + c);
                    FileInputStream fis = new FileInputStream(in);
                    try {
                        while ((readBytesCount = fis.read(buffer)) != -1) {
                            fos.write(buffer, 0, readBytesCount);
                        }
                    } finally {
                        fis.close();
                    }
                }
            } finally {
                fos.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}

class ProcessFailureException extends Exception {
    public ProcessFailureException(String message) {
        super(message);
    }
}
